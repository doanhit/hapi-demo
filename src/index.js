"use strict";
const Hapi = require('hapi');

const server = Hapi.server({port:3000, host:"localhost"});
const init = async () =>{
    await server.start()
;
console.log(`Server running at ${server.info.uri}`)
};
init().catch(err =>{console.error("An error happen",{err,});

process.exit(1);
});


server.route(require('./routes'));