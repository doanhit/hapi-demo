"use strict";
const userHandler = require('./handlers/users');

module.exports = [{
    method: 'GET',
    path: '/users',
    handler: userHandler.find
}, {
    method: 'GET',
    path: '/users/{id}',
    handler: userHandler.update
}] ;